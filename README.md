# Expression du besoin du Conseil Départemental du Nord

## Objet du document

Ce texte a pour objet de synthétiser l'expression de besoin du Conseil Départemental du Nord (CD59) concernant l'application Départements & Notaires (D&N)

## 5 Besoins résumés

1. Donner une information claire et exhaustive au notaire quant aux aides et donc aux potentielles créances concernant un usager. L'usager peut être connu pour une ou plusieurs aides.
    * Afficher dans les courriers PDF transmis aux notaires les aides pour lesquelles l’usager est connu, ainsi que les coordonnées du/des service(s) gestionnaire(s) (3 services différents au sein du CD59), ce dernier variant selon les aides servies
    * Envoyer aux notaires un seul courrier PDF listant de manière exhaustive toutes les aides servies à l’usager, pour bloquer sa succession. 
1. Pouvoir personnaliser les courriers à la charte graphique du département
1. Gérer les adresses des études et un destinataire nommé de l'étude dans l'adressage des courriers
1. Afficher les identifiants IODAS (N° dossier et N° individu) ainsi que les dates de naissance et de décès en référence dans les courriers
1. Permettre un export des résultats de recherche.

## Besoin 1: Expression technique et piste de mise en oeuvre

Au sein du CD59, les aides aux personnes agées et aux personnes handicapées ne sont pas toutes traitées par le même service. En pratique trois services différents gèrent les aides. Exemple : 

* APA établissement --> Service 1
* Allocation individuelle (APA domicile, PCH, ACTP) --> Service 2
* Aide Sociale Générale (Hébergement en établissement, accueil familial, aide ménagère, portage de repas) --> Service 3

### Traitement type d'une demande *sans* l'application D&N

1. Le Service 2 (allocation individuelle) reçoit toutes les demandes, vérifie la présence de la personne dans la base de donnée : 
    * En cas d'absence (85% des cas), le Service 2 renvoie un courrier (type inconnu) au notaire indiquant qu'il peut continuer sa succession, le traitement est terminé coté CD59.
    * Sinon le Service 2 envoie un courrier d'attente au notaire en lui précisant que l'usager est connu et que les services du CD59 vont revenir vers lui avec des informations complémentaires. Remarque : la présence dans la base ne préjuge **pas forcément** de la présence d'une créance à récupérer, simplement que la personne est connue du CD59.
1. Lorsque le Service 2 détecte que l'usager est connu dans la base pour une ou plusieurs aides ou allocation individuelle, il transmet au service concerné la demande et garde pour instruction les demandes qui le concernent. En fonction du nombre d’aides servies, un ou plusieurs courriers d’opposition seront envoyés aux notaires par chacun des 3 services gestionnaires.
1. Les trois services calculent le montant des éventuelles créances et envoient les informations par courrier au notaire. Cette étape est faite, **en parallèle** dans les trois services et **indépendemment** du résultat des deux autres services. Dit autrement, un notaire peut recevoir jusqu'à trois courriers du CD59. De plus, un service n'est pas forcément informé qu'un des deux autres a identifié une créance. Les opérations des 3 services sont distinctes et ne se font pas dans la même temporalité.

### Limites du traitement actuel

* Le notaire ne sait pas combien de courrier du CD il doit attendre.
* Dans certains cas fréquents, deux courriers sont envoyés au notaire. Le premier 3 jours après la demande, mentionnant une créance de 100€ (indu APA Dom) ; le deuxième 3 mois après la demande, mentionnant une créance de 10'000€ (récupération ASG). Si le notaire a cloturé la succession entre les deux courriers, alors la deuxième créance ne pourra plus être récupérée dans le cadre de la succession, ce qui obligera les services du Département à poursuivre le recouvrement auprès des héritiers. 
* Cela met donc en difficulté les services du Département, les héritiers qui ont reçu et peut-être utilisé leur part d’héritage, les notaires qui n’avaient pas une information exhaustive au moment de la clôture et qui doivent faire face aux protestations des héritiers.
* Pour éviter cela, l’interêt est donc d’expliciter de manière exhaustive dès le premier courrier la liste des aides pour lesquelles le Département fait opposition à la clôture de la succession.


### Proposition d'évolution de l'application D&N

* Présenter dès le premier courrier PDF envoyé au notaire :
    * la liste exhaustive des aides concernant la personne
    * les coordonnées de chaque service en face de chaque aide

### Suggestions de pistes techniques pour l'architecture technique

**Remarque :** les idées présentées ci-après portent sur des suggestions *d'architecture*, les détails *d'implémentation* seront vus ultérieurement.

Le CD59 dispose d'un "catalogue d'aides" représentant une soixantaine de ligne. Ceci peut être simplifié en un tableau de quelques lignes. La création de ce "tableau concentré des aides" relève de la configuration sur site de chaque installation de l'application D&N.

L'application doit avoir connaissance de la liste des aides (si l'on souhaite pouvoir les placer sur le PDF). Cette liste pouvant varier d'un CD à l'autre, elle doit être saisie dans un fichier de configuration.

De la même manière, les noms et coordonnées de différents services doivent aussi être connus de l'application. Étant différents d'un CD à l'autre, cette liste doit être saisie dans un fichier de configuration.

Enfin la correspondance établissant quel service gérant quelle aide doit aussi être connue de l'application. Cette dernière doit aussi être saisie dans un fichier de configuration.

Ces informations doivent permettre d’être utilisées pour dispacher les mails directement sur les bons services gestionnaires qui doivent être en copie des mails aux notaires. Ce traitement permettra de remplacer le dispatch aujourd’hui fait de manière manuelle, par un traitement complètement automatique. 


## Besoin 2: Expression technique et piste de mise en oeuvre

### Proposition d'évolution de l'application D&N
le département du Nord applique une charte graphique spécifique (police, positionnement du logo, des cartouches, ...).

L'application devrait faciliter l'implémentation d'une charte graphique pour les mails et courriers envoyés

### Suggestions de pistes techniques pour l'architecture technique

A étudier

## Besoin 3: Expression technique et piste de mise en oeuvre

### Proposition d'évolution de l'application D&N
Gérer le pavé adresse dans les courriers envoyés en copie des mails
Faciliter la dispatching au sein d'une étude, des réponses envoyés en mentionnant un destinataire nommé de l'étude.

### Suggestions de pistes techniques pour l'architecture technique

* Enrichir les comptes notaires avec l'adresse postale de l'étude
* Permettre lors d'une recherche la saisie d'un destinataire de la réponse au sein de l'étude à reprendre dans les mails et courriers

## Besoin 4: Expression technique et piste de mise en oeuvre

### Proposition d'évolution de l'application D&N

* Rappeler les critères de recherche de l'étude et mentionner la date de décès saisie sur les mails et courriers 
* Préciser sur les mails et courriers le N° de dossier IODAS et l'identifiant individu IODAS ayant fait l'objet de la réponse (si connu)

### Suggestions de pistes techniques pour l'architecture technique

* Alimenter la base de données des données manquantes 
* Permettre l'utilisation de ces données et des données de la recherche dans les mails et courriers
 
## Besoin 5: Expression technique et piste de mise en oeuvre

### Proposition d'évolution de l'application D&N

Permettre un export des résultats de recherche à des fins statistiques et de traitement de la date de décès dans IODAS.

### Suggestions de pistes techniques pour l'architecture technique

* Enrichir l'historique de recherches des types d'aides (si réponse = connu)
* Permettre une extraction au format xls de l'historique